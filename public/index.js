"use strict";

var socket = io.connect('http://localhost:8080', {
  'forceNew': true
});
var videoElem = document.getElementById('video-player');
var videoStatus = "paused";
socket.on('PlayVideo', function (data) {
  if (videoStatus !== "playing") {
    videoElem = document.getElementById('video-player');
    videoElem.currentTime = data.videoTime;
    videoStatus = "playing";
    videoElem.play();
  }
});
socket.on('StopVideo', function (data) {
  if (videoStatus !== "paused") {
    videoElem.currentTime = data.videoTime;
    videoStatus = "paused";
    videoElem.pause();
  }
});
socket.on('MoveVideo', function (data) {
  if (videoStatus !== "moved") {
    videoStatus = "moved";
    videoElem.currentTime = data.videoTime;
  }
});
videoElem.addEventListener('play', function (e) {
  if (videoStatus !== "playing") {
    videoStatus = "playing";
    socket.emit('PlayVideo', {
      videoTime: videoElem.currentTime
    });
  }
});
videoElem.addEventListener('pause', function (e) {
  if (videoStatus !== "paused") {
    videoStatus = "paused";
    socket.emit('StopVideo', {
      videoTime: videoElem.currentTime
    });
  }
});
videoElem.addEventListener('seeked', function (e) {
  if (videoStatus !== "moved") {
    videoStatus = "moved";
    socket.emit('MoveVideo', {
      videoTime: videoElem.currentTime
    });
  }
});