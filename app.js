var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var videoStatus = "paused";

app.use(express.static('public'));

app.get('/hola', (req, res) => {
    res.status(200).send("Hello");
});

io.on('connection', (socket) => {
    socket.addListener("PlayVideo", (data) => {
        videoStatus = "playing";
        socket.broadcast.emit('PlayVideo', {
            videoTime: data.videoTime
        });
    });
    socket.addListener("StopVideo", (data) => {
        videoStatus = "paused";
        socket.broadcast.emit('StopVideo', {
            videoTime: data.videoTime
        });
    });
    socket.addListener("MoveVideo", (data) => {
        videoStatus = "moved";
        socket.broadcast.emit('MoveVideo', {
            videoTime: data.videoTime
        });
    });
})

server.listen('8080', () => {
    console.log('el servidor corre en puerto 8080');
})