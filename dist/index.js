var socket = io.connect('http://localhost:8080', { 'forceNew': true });
var videoElem = document.getElementById('video-player');
var videoStatus = "paused";

socket.on('PlayVideo', (data) => {
    if(videoStatus !== "playing"){
        videoElem = document.getElementById('video-player');
        videoElem.currentTime = data.videoTime;
        videoStatus = "playing";
        videoElem.play();
    }
});

socket.on('StopVideo', (data) => {
    if(videoStatus !== "paused"){
        videoElem.currentTime = data.videoTime;
        videoStatus = "paused";
        videoElem.pause();
    }
});

socket.on('MoveVideo', (data) => {
    if(videoStatus !== "moved"){
        videoStatus = "moved";
        videoElem.currentTime = data.videoTime;
    }
});

videoElem.addEventListener('play', (e) => {
    if(videoStatus !== "playing"){
        videoStatus = "playing";
        socket.emit('PlayVideo', {
            videoTime: videoElem.currentTime
        });
    }
});

videoElem.addEventListener('pause', (e) => {
    if(videoStatus !== "paused"){
        videoStatus = "paused";
        socket.emit('StopVideo', {
            videoTime: videoElem.currentTime
        });
    }
});

videoElem.addEventListener('seeked', (e) => {
    if(videoStatus !== "moved"){
        videoStatus = "moved";
        socket.emit('MoveVideo', {
            videoTime: videoElem.currentTime
        });
    }
})